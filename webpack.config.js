const Path = require('path');

module.exports = {
  mode: process.env.NODE_ENV,
  target: 'node',
  entry: {
    index: [
      Path.join(__dirname, 'src', 'index.js')
    ]
  },
  output: {
    libraryTarget: 'umd',
    library: '@jitesoft/cli'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        loader: 'babel-loader'
      }
    ]
  }
};
