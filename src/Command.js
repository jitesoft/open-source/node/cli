/**
 * @typedef {Object} Command~Options
 * @property {Array<String>} [aliases] List of aliases for the given command. Defaults to `[]`.
 * @property {Array<Option>|Collection<Option>} [options] List of options available to use. Defaults to `[]`.
 * @property {Array<Argument>|Collection<Argument>} [args] List of arguments used in command. Defaults to `[]`.
 */

/**
 * Callback used instead of the Command.handle class method.
 * @typedef {Function} Command~Handle
 * @param {Command} command The command called (this).
 * @param {Input} input Input handler.
 * @param {Array<Arguments>} args List of arguments used in input.
 * @param {Array<Option>} options List of options used in input.
 * @returns {Promise<*>}
 */

const optionNames = ['aliases', 'options', 'args', 'examples'];

/**
 * Each command is represented by a given command class. The class is intended to be overridden by specific commands that
 * implement the `invoke` method.
 * When a command does not need the class as such, a callback can be passed to be used instead of implementing the handler.
 */
export default class Command {
  /** @var {String} */
  _description;
  /** @var {String} */
  _name;
  /** @var {Array<string>} */
  _aliases;
  /** @var {Array<Option>} */
  _options;
  /** @var {Array<Argument>} */
  _args;
  /** @var {Array<string>} */
  _examples;
  /** @var {Command~Handle} */
  _callback;

  /**
   *
   * @param {String}          name        Name of the command.
   * @param {String}          description Description of the command.
   * @param {Command~Options} [options]   Options the command utilizes.
   * @param {Command~Handle}  [callback]  Handler callback to use instead of the `handle` class instance method.
   */
  constructor (name, description, options = {}, callback = null) {
    for (const key of optionNames) {
      if (key in options) {
        this[`_${key}`] = options[key];
      } else {
        this[`_${key}`] = [];
      }
    }

    this._name = name;
    this._description = description;
    this._callback = callback;
  }

  /**
   * Handle the command.
   *
   * @abstract
   * @param {Command}                              command The command called (this).
   * @param {Input}                                input   Input handler.
   * @param {Array<Argument>|Collection<Argument>} args    List of arguments used in input.
   * @param {Array<Option>|Collection<Option>}     options List of options used in input.
   * @returns {Promise<*>}
   */
  async handle (command, input, args = [], options = []) {
    if (this._callback !== null) {
      return Promise.resolve().then(() => this._callback(command, input, args, options));
    }

    return Promise.reject(new Error('Abstract method.'));
  }

  /**
   * Get aliases for given command.
   *
   * @readonly
   * @returns {Array<String>}
   */
  get aliases () {
    return this._aliases;
  }

  /**
   * Get arguments used by the command.
   *
   * @readonly
   * @returns {Array<Argument>}
   */
  get args () {
    return this._args;
  }

  /**
   * Get examples for given command.
   *
   * @readonly
   * @returns {Array<String>}
   */
  get examples () {
    return this._examples;
  }

  /**
   * Get the command name.
   *
   * @readonly
   * @returns {String}
   */
  get name () {
    return this._name;
  }

  /**
   * Get the description of the command.
   *
   * @readonly
   * @returns {String}
   */
  get description () {
    return this._description;
  }

  /**
   * Get the options used by the command.
   *
   * @readonly
   * @returns {Array<Option>}
   */
  get options () {
    return this._options;
  }

  /**
   * @return {String}
   * @private
   */
  _signature () {
    const opts = this._options.map((opt) => {
      const alts = opt.aliases.map(a => `-${a}`).join('|');
      return `--${opt.name}` + (alts.length > 0 ? `[${alts}]` : '');
    }).join(' ');

    const requiredArgs = this._args.filter((a) => a.required).map((a) => {
      return `${a.name}`;
    }).join(' ');

    const optionalArgs = this.args.filter((a) => !a._required).map((a) => {
      return `{${a.name}}`;
    }).join(' ');

    return `${this._name} ${requiredArgs} ${optionalArgs} ${opts}`;
  }

  /**
   *
   * @param managerName
   * @return {String}
   * @private
   */
  _signatureLong (managerName) {
    let sign = `${this._name}: ${this._description}\n`;
    sign += `\nUsage: ${managerName} ${this.name} ${this._options.length > 0 ? '[options]' : ''} ${this._args.map(a => a.namePascalCase).join(' ')}\n`;

    if (this._aliases.length > 0) {
      sign += `\nAliases: ${this._aliases.join(', ')}\n`;
    }

    if (this._args.length > 0) {
      sign += 'Arguments:\n' +
        `${this._args.filter(a => a.required).map(a => `\t${a._signature()}`).join('\n')}\n` +
        `${this._args.filter(a => !a.required).map(a => `\t${a._signature()}`).join('\n')}\n`;
    }

    if (this._options.length > 0) {
      sign += `Options:\n${this._options.map(o => `\t${o._signature()}`).join('\n')}`;
    }

    return sign;
  }
}
