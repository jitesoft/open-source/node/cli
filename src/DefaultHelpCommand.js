import Command from './Command';
import Argument from './Argument';

/**
 * Default help command. Automatically registered and can be swapped by registering a new help.
 */
export default class Help extends Command {
  _manager;

  /**
   * @param {Manager} manager
   */
  constructor (manager) {
    super('help', 'Prints this help text.');
    this._manager = manager;
    this._args.push(new Argument('command', 'Command to get detailed help for.', false));
  }

  // noinspection JSCheckFunctionSignatures
  /**
   * Handle the command.
   *
   * @abstract
   * @param {Command}                              command The command called (this).
   * @param {Input}                                input   Input handler.
   * @param {Array<Argument>|Collection<Argument>} args    List of arguments used in input.
   * @param {Array<Command>|Collection<Command>}   commands List of commands in the engine.
   * @returns {Promise<*>}
   */
  async handle (command, input, args, commands) {
    if (args.length > 0) {
      const cmd = args[0].toLowerCase();
      const c = commands.find((c) => c.name.toLowerCase() === cmd);

      if (!c) {
        throw new Error(`Failed to find the command "${cmd}".`);
      }

      return Promise.resolve(c._signatureLong(this._manager._name));
    }

    let sign = `${this._manager._name}: ${this._manager._description}`;
    sign += `\nUsage: ${this._manager._name} {command} [arguments] [options] `;
    sign += `\n\nAvailable commands:\n${commands.map((c) => `\t${c._name}\t- ${c._description}`).join('\n')}`;
    sign += '\n\nRun help {command} for more information about a given command.';

    return Promise.resolve(sign);
  }
}
