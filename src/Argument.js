/**
 * A structure containing data for a single argument.
 */
export default class Argument {
  /** @var {String} */
  _name;
  /** @var {String} */
  _description;
  /** @var {*} */
  _expectedType;
  /** @var {Boolean} */
  _required;
  /** @var {String|null} */
  _value;

  /**
   * @param {String}  name           Name of the argument.
   * @param {String}  description    Short description of argument.
   * @param {Boolean} [required]     If argument is a required argument or not.
   * @param {*}       [expectedType] Expected type (pass constructor).
   */
  constructor (name, description, required = true, expectedType = null) {
    this._name = name;
    this._expectedType = expectedType;
    this._description = description;
    this._required = required;
    this._value = null;
  }

  /**
   * @readonly
   * @return {String}
   */
  get name () {
    return this._name;
  }

  /**
   * Get the name as PascalCase.
   * @readonly
   * @return {String}
   */
  get namePascalCase () {
    const name = this._name.replace(/(\w)(\w*)/g, (_, b, c) => `${b.toUpperCase()}${c.toLowerCase()}`);
    return name.replace(' ', '');
  }

  /**
   * @readonly
   * @return {String}
   */
  get description () {
    return this._description;
  }

  /**
   * @readonly
   * @return {*}
   */
  get expectedType () {
    return this._expectedType;
  }

  /**
   * @readonly {*}
   * @return {Boolean}
   */
  get required () {
    return this._required;
  }

  /**
   * @internal
   * @param value
   */
  set value (value) {
    this._value = value;
  }

  /**
   * @readonly
   * @return {String}
   */
  get value () {
    return this._value;
  }

  /**
   * @return {String}
   * @private
   */
  _signature () {
    let sign = `${this._required ? this._name : `[${this._name}]`}\t`;
    sign += this._expectedType !== null ? `[${this._expectedType.name}]\t` : '\t\t';
    sign += `- ${this._description}`;
    return sign;
  }
}
