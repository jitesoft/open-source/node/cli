import Input from './Input';
import Help from './DefaultHelpCommand';
import Collection from './Collection';

/**
 * Main class for the CLI api.
 */
export default class Manager {
  /** @var {Array<Command>} */
  _commands;
  /** @var {String} */
  _name;
  /** @var {String} */
  _description;
  /** @var {Input} */
  _input;
  /** @var {null|Command} */
  _help = null;

  constructor (name, description, input = null, output = null) {
    this._commands = [];
    this._name = name;
    this._description = description;
    this._input = new Input(input, output);
    this.registerHelp();
  }

  /**
   * Register another help command than the currently used one.
   *
   * @param command
   * @return {*}
   */
  registerHelp (command = null) {
    if (command) {
      const index = this._commands.findIndex((o) => o.name.toLowerCase() === 'help');
      if (index > -1) {
        this._commands[index] = command;
        return;
      }

      return this.register(command);
    }

    this._help = new Help(this);
    this.register(this._help);

    return this;
  }

  /**
   * Register a command to the manager.
   * @param {Command} command
   * @return {Manager} self.
   */
  register (command) {
    if (this._commands.some(c => c.name.toLowerCase() === command.name.toLowerCase() || c._aliases.some(a => a.toLowerCase() === command.name.toLowerCase()))) {
      throw new Error(`Command with name ${command.name} is already registered.`);
    }

    this._commands.push(command);
    return this;
  }

  /**
   * Get command count.
   *
   * @readonly
   * @returns {number}
   */
  get count () {
    return this._commands.length;
  }

  /**
   * Get command count.
   *
   * @readonly
   * @returns {number}
   */
  get length () {
    return this._commands.length;
  }

  /**
   * Invoke manager.
   *
   * @param {Array<*>} args
   * @returns {Promise<*>}
   */
  async invoke (args = process.argv) {
    args = args.splice(2);
    const realName = args.length > 0 ? args[0] : 'help';
    const name = realName.toLowerCase();
    const cmd = this._commands.find(
      c => c.name.toLowerCase() === name || c.aliases.some(s => s.toLowerCase() === name)
    );
    args = args.splice(1);

    if (cmd instanceof Help) {
      return cmd.handle(cmd, this._input, args, this._commands);
    }

    if (cmd) {
      return cmd.handle(
        cmd,
        this._input,
        Manager._getArgs(args, cmd),
        Manager._getOptions(args, cmd)
      );
    }
    return Promise.reject(new Error(`Command ${realName} does not exist.`));
  }

  /**
   * @param {Array<string>} argArr  Argument list.
   * @param {Command}       command Command which is invoked.
   * @private
   */
  static _getOptions (argArr, command) {
    const options = argArr.map((o, index, array) => {
      if (o.indexOf('-') !== 0) {
        return null; // Throw away arguments which are not options.
      }

      let value = null;
      let options = [];
      o = o.substr(1); // Remove the fist `-`.
      if (o.indexOf('=') !== -1) {
        [o, value] = o.split('=');
        value = value?.replace(/"/gi, '');
      }

      // Case 1. No `-`, then it's a short version, i.e., a single -. They are treated as aliases always.
      // In case there are more than value, it will now be mapped into an array of options, else just into an array with one option.
      // Case 2. Still a `-`. Then it is a long option, i.e., a double --. They are treated as names always.
      // There is always only one option when using a name, so it is made into a list of one option.
      o = o.toLowerCase();
      if (o.indexOf('-') !== 0) {
        options = o.split('').map((alias) => command.options.find((opt) =>
          opt.aliases.map(a => a.toLowerCase()).indexOf(alias) !== -1)
        );
      } else {
        o = o.substr(1);
        options = [command.options.find(opt => opt.name.toLowerCase() === o)];
      }

      if (options.some(opt => (opt === undefined || opt === null))) {
        throw new Error(`Option \`t\` is not defined. Run \`${command.name} --help\` for valid options.`);
      }

      if (value !== null) {
        return options.map(opt => {
          opt.value = value;
          return opt;
        });
      }

      if (options.some(opt => opt.requireValue)) {
        if (array.length < (index + 2)) {
          throw new Error(`Expected value for option \`${options[0].name}\`, but found no value.`);
        }

        value = array[index + 1].replace(/"/gi, '');
        // Take next item!
        return options.map(opt => {
          opt.value = value;
          return opt;
        });
      }

      return options;
    }).flat().filter(o => o !== null);

    return new Collection(options);
  }

  static _getArgs (argArr, command) {
    // An argument is a value only, either with " or without, if no ", each word will count as a new argument.
    // Arguments can be either optional or required. If optional, they will be AFTER the required in the command,
    // important to remember though: If the required arguments are not filled, it's an error, if the required are filled
    // and the optional are used, they need to be in the correct order.
    // Order is depending on the order they where added to the command.
    const required = command.args.filter(a => a.required);
    const optional = command.args.filter(a => !a.required);
    const allArguments = required.concat(optional);
    const len = argArr.length;

    let arg = null;
    const args = [];

    for (let i = 0; i < len; i++) {
      arg = argArr[i];
      if (arg.indexOf('-') === 0) {
        break; // All done if it's an option.
      }

      // If it's not an option, we remove `"` in case there are any.
      arg = arg.replace(/"/gi, '');
      if (i + 1 > allArguments.length) {
        throw new Error(`Unknown argument \`${arg}\`.`);
      }

      const argument = allArguments[i];
      argument.value = arg;
      args.push(argument);
    }

    if (args.length < required.length) {
      throw new Error(`Missing required argument \`${required[args.length].name}\`.`);
    }

    return new Collection(args);
  }
}
