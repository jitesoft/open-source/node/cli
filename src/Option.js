/**
 * A structure containing data for a single option (--option).
 */
export default class Option {
  /** @var {String} */
  _name;
  /** @var {Array<String>} */
  _aliases;
  /** @var {*} */
  _expectedType;
  /** @var {String} */
  _description;
  /** @var {Boolean} */
  _requireValue;
  /** @var {*} */
  _value;

  /**
   * @param {String}        name            Name of the option.
   * @param {String}        description     Description of the option.
   * @param {Array<String>} [aliases]       Option Aliases (will use - instead of --).
   * @param {Boolean}       [requireValue]  If a value is required when using option.
   * @param {*}             [expectedType]  Type expected when value is passed (constructor).
   */
  constructor (name, description = '', aliases = [], requireValue = false, expectedType = null) {
    this._name = name;
    this._description = description;
    this._expectedType = expectedType;
    this._aliases = aliases ?? [];
    this._value = null;
    this._requireValue = requireValue;
  }

  /**
   * Get the name of the option.
   *
   * @readonly
   * @returns {String}
   */
  get name () {
    return this._name;
  }

  /**
   * Get the name as PascalCase.
   * @readonly
   * @return {String}
   */
  get namePascalCase () {
    const name = this._name.replace(/(\w)(\w*)/g, (_, b, c) => `${b.toUpperCase()}${c.toLowerCase()}`);
    return name.replace(' ', '');
  }

  /**
   * Get aliases for the given option.
   *
   * @readonly
   * @returns {Array<String>}
   */
  get aliases () {
    return this._aliases;
  }

  /**
   * Get the expected type of the option.
   *
   * @readonly
   * @internal
   * @returns {*}
   */
  get expectedType () {
    return this._expectedType;
  }

  /**
   * Get the description of the option.
   *
   * @readonly
   * @return {String}
   */
  get description () {
    return this._description;
  }

  /**
   * @internal
   * @return {Boolean}
   */
  get requireValue () {
    return this._requireValue;
  }

  /**
   * Get the value.
   *
   * @return {*}
   */
  get value () {
    return this._value;
  }

  /**
   * Set value
   *
   * @internal
   * @param {*} value
   */
  set value (value) {
    this._value = value;
  }

  /**
   * @return {String}
   * @private
   */
  _signature () {
    let sign = `--${this._name}\t`;
    sign += this._aliases.length > 0 ? `(-${this._aliases.join('')})\t` : '\t';
    sign += this._expectedType !== null ? `[${this._expectedType.name}]\t` : ' \t';
    sign += `- ${this._description}`;
    return sign;
  }
}
