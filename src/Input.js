import readline from 'readline';

/**
 * Input wrapper. Allows for fetching `line` (single line of input) and to query for input via
 * the `question` method.
 */
export default class Input {
  _reader;
  _writer;

  /**
   *
   * @param {NodeJS.ReadableStream} input
   * @param {NodeJS.WritableStream} output
   */
  constructor (input, output) {
    this._writer = output || process.stdout;
    if (process.env.NODE_ENV !== 'test') {
      this._reader = readline.createInterface({
        input: input || process.stdin,
        output: this._writer,
        terminal: false
      });
    }
  }

  /**
   * Prompt for input.
   *
   * @returns {Promise<String>} Response as promise resolving to string.
   */
  async line () {
    return new Promise((resolve, reject) => {
      this._reader.on('line', (line) => {
        return resolve(line);
      });
    });
  }

  /**
   * Ask a question and wait for response.
   *
   * @param {String} query Question.
   * @returns {Promise<String>} Response as promise resolving to string.
   */
  async question (query) {
    await this.output(`${query}`);
    return this.line();
  }

  /**
   * Write to output stream.
   *
   * @param value
   * @return {Promise<void>}
   */
  async output (value) {
    return new Promise((resolve, reject) => {
      this._writer.write(`${value}\n`, (e) => {
        if (e) {
          return reject(e);
        }
        resolve();
      });
    });
  }

  /**
   * Query user and await input which have to evaluate to either passed value.
   * The values passed in to evaluate against will be converted to strings and set to lower case, and the input
   * will be evaluated in lowercase too. The original input will be the same as the passed value when resolved.
   *
   * @param {String}        query   Question.
   * @param {Array<String>} options Options to provide.
   * @param {String}        error   Message to output in case of invalid value.
   * @return {Promise<String>}
   */
  async option (query, options, error) {
    const opts = options.map(o => `${o}`.toLowerCase());
    let response = null;

    for (; ;) {
      response = (await this.question(query)).toLowerCase();
      response = opts.findIndex(elem => elem === response);

      if (response && response !== -1) {
        return options[response];
      }
    }
  }

  /**
   * Ask a question, if no value the `or` param will be returned.
   *
   * @param {String} query Question.
   * @param {*}      or    Default result.
   * @return {Promise<String>}
   */
  async questionOr (query, or) {
    const response = await this.question(`${query} [${or}]`);
    return response === '' ? or : response;
  }

  /**
   * Await input, if no value the `or` parameter will be returned.
   *
   * @param {String} or Default result.
   * @return {Promise<String>}
   */
  async lineOr (or) {
    const response = await this.line();
    return response === '' ? or : response;
  }
}
