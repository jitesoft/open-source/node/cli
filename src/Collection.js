export default class Collection extends Array {
  constructor (array) {
    super();
    this.push(...array);
  }

  /**
   * Check if a given object exist by `name`.
   * @param name
   * @return {Boolean}
   */
  has (name) {
    return this.some(o => o.name === name);
  }

  /**
   * Get a given value by `name`.
   * @param {String} name Name of object to check for.
   * @param {*}      def  Default object to return if not exist.
   * @return {*}
   */
  get (name, def) {
    return this.find(o => o.name === name) ?? def;
  }
}
