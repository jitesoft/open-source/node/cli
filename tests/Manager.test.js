import { Manager, Command } from '../src';

jest.mock('../src/Input');

describe('Manager tests.', () => {
  let _manager;
  beforeEach(() => {
    _manager = new Manager('TestManager', 'A manager used for testing.');
  });

  test('Register should add command to manager.', () => {
    expect(_manager).toHaveLength(1); // Help command always present.
    _manager.register(new Command('TestCommand', 'TestDescription'));
    expect(_manager).toHaveLength(2);
    _manager.register(new Command('TestCommand2', 'TestDescription'));
    expect(_manager).toHaveLength(3);
    expect(() => { _manager.register(new Command('TestCommand', 'TestDescription')); }).toThrow(Error);
  });

  describe('Invokation tests.', () => {
    test('Rejection with an error if command does not exist.', async () => {
      await expect(_manager.invoke(['node', '@jitesoft/cli', 'Abc'])).rejects.toThrow('Command Abc does not exist.');
    });

    test('Command should succeed without options or arguments.', async () => {
      let called = false;

      class TestCmd extends Command {
        async handle (command, input, args, options) {
          called = true;
          expect(command.name).toEqual('TestCommand');
          expect(args).toEqual([]);
          expect(options).toEqual([]);
          return Promise.resolve();
        }
      }

      const cmd = new TestCmd('TestCommand', 'e');
      _manager.register(cmd);
      await _manager.invoke([
        'node',
        '@jitesoft/cli',
        'TestCommand'
      ]);
      expect(called).toEqual(true);
    });

    test('Command should succeed with a callback handler.', async () => {
      let called = false;
      const callback = async (command, input, args, options) => {
        called = true;
        expect(command.name).toEqual('TestCommand');
        expect(args).toEqual([]);
        expect(options).toEqual([]);
        return Promise.resolve();
      };

      const cmd = new Command('TestCommand', 'e', {}, callback);
      _manager.register(cmd);
      await _manager.invoke([
        'node',
        '@jitesoft/cli',
        'TestCommand'
      ]);
      expect(called).toEqual(true);
    });
  });
});
