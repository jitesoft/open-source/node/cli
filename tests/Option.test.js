import Option from '../src/Option';
import Command from '../src/Command';
import Manager from '../src/Manager';

describe('Options tests.', () => {
  describe('Test Setters/Getters', () => {
    const opt = new Option('TestOpt', 'Test description!', [
      'abc',
      'efg',
      'test'
    ], true, String, true);

    test('Name', () => {
      expect(opt.name).toEqual('TestOpt'); // Always converted to lower string.
    });

    test('Aliases', () => {
      expect(opt.aliases).toHaveLength(3);
      expect(opt.aliases).toEqual(expect.arrayContaining(['abc', 'efg', 'test']));
    });

    test('Expected type', () => {
      expect(opt.expectedType).toEqual(String);
    });

    test('Description', () => {
      expect(opt.description).toEqual('Test description!');
    });

    test('Value', () => {
      expect(opt.value).toBeNull();
      opt.value = 'abc123';
      expect(opt.value).toEqual('abc123');
    });
  });

  describe('Test handle command with options.', () => {
    let _manager;
    let called = false;

    beforeEach(() => {
      _manager = new Manager('TestManager', 'A manager used for testing.');
      called = false;
    });

    afterEach(() => {
      expect(called).toEqual(true);
    });

    test('Long option with double - should succeed.', async () => {
      const opt = new Option('test', 'Test option.');
      const cmd = new Command('TestCommand', 'Test command.', { options: [opt] }, async (c, i, a, o) => {
        called = true;
        expect(o).toHaveLength(1);
        expect(o[0].name).toEqual('test');
        return Promise.resolve();
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '--test'
      ]);
    });

    test('Short option with single - should succeed', async () => {
      const opt = new Option('test', 'Test option.', ['t']);
      const cmd = new Command('TestCommand', 'Test command.', { options: [opt] }, async (c, i, a, o) => {
        called = true;
        expect(o).toHaveLength(1);
        expect(o[0].name).toEqual('test');
        return Promise.resolve();
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '-t'
      ]);
    });

    test('Short option with double - should fail.', async () => {
      const opt = new Option('test', 'Test option.', ['t']);
      const cmd = new Command('TestCommand', 'Test command.', { options: [opt] }, (c, i, a, o) => {});
      called = true; // Just to skip afterEach to fail.
      _manager.register(cmd);
      await expect(_manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '--t'
      ])).rejects.toThrow('Option `t` is not defined. Run `TestCommand --help` for valid options.');
    });

    test('Long option with a single - should be treated as multiple short options.', async () => {
      const opt1 = new Option('test', 'Test option.', ['t']);
      const opt2 = new Option('test2', 'Test option.', ['h']);
      const opt3 = new Option('h', 'Test option.');

      const cmd = new Command('TestCommand', 'Test command.', { options: [opt1, opt2, opt3] }, async (c, i, a, o) => {
        called = true;

        expect(o).toHaveLength(2);
        expect(o[0].name).toEqual('test');
        expect(o[1].name).toEqual('test2');
        return Promise.resolve();
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '-th'
      ]);
    });

    test('Option is optional.', async () => {
      const opt1 = new Option('test', 'Test option.', ['t']);
      const opt2 = new Option('test2', 'Test option.', ['h']);
      const opt3 = new Option('h', 'Test option.');

      const cmd = new Command('TestCommand', 'Test command.', { options: [opt1, opt2, opt3] }, async (c, i, a, o) => {
        called = true;

        expect(o).toHaveLength(1);
        expect(o[0].name).toEqual('test');
        return Promise.resolve();
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '-t'
      ]);
    });

    test('Option requires value.', async () => {
      const opt1 = new Option('test', 'Test option.', ['t'], true);
      const opt2 = new Option('test2', 'Test option.', ['h'], true);

      const cmd = new Command('TestCommand', 'Test command.', { options: [opt1, opt2] }, async (c, i, a, o) => {
        return Promise.resolve();
      });
      called = true; //
      _manager.register(cmd);
      await expect(_manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '-th'
      ])).rejects.toThrow('Expected value for option `test`, but found no value.');
    });

    test('Option with value and no = succeeds.', async () => {
      const opt1 = new Option('test', 'Test option.', ['t'], true);

      const cmd = new Command('TestCommand', 'Test command.', { options: [opt1] }, async (c, i, a, o) => {
        expect(opt1.value).toEqual('hejpådej');
        expect(opt1.name).toEqual('test');
        called = true;
        return Promise.resolve();
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '-t', 'hejpådej'
      ]);
    });

    test('Option with value and = succeeds.', async () => {
      const opt1 = new Option('test', 'Test option.', ['t'], true);

      const cmd = new Command('TestCommand', 'Test command.', { options: [opt1] }, async (c, i, a, o) => {
        expect(opt1.value).toEqual('hejpådej');
        expect(opt1.name).toEqual('test');
        called = true;
        return Promise.resolve();
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '-t=hejpådej'
      ]);
    });

    test('Option with multi word value and = will treat only first word as value.', async () => {
      const opt1 = new Option('test', 'Test option.', ['t'], true);

      const cmd = new Command('TestCommand', 'Test command.', { options: [opt1] }, async (c, i, a, o) => {
        called = true;
        expect(o[0].value).toEqual('hejpådej');
        return Promise.resolve();
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '-t=hejpådej', 'hur', 'mår', 'du'
      ]);
    });

    test('Option with multi word value without = and no " to treat only first word as value.', async () => {
      const opt1 = new Option('test', 'Test option.', ['t'], true);

      const cmd = new Command('TestCommand', 'Test command.', { options: [opt1] }, async (c, i, a, o) => {
        called = true;
        expect(o[0].value).toEqual('hejpådej');
        return Promise.resolve();
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '-t', 'hejpådej', 'hur', 'mår', 'du'
      ]);
    });

    test('Option with multi word value and " succeeds.', async () => {
      const opt1 = new Option('test', 'Test option.', ['t'], true);

      const cmd = new Command('TestCommand', 'Test command.', { options: [opt1] }, async (c, i, a, o) => {
        expect(opt1.value).toEqual('hejpådej hur mår du?');
        expect(opt1.name).toEqual('test');
        called = true;
        return Promise.resolve();
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '-t', '"hejpådej hur mår du?"'
      ]);
    });

    test('Option with multi word value, " and = succeeds.', async () => {
      const opt1 = new Option('test', 'Test option.', ['t'], true);

      const cmd = new Command('TestCommand', 'Test command.', { options: [opt1] }, async (c, i, a, o) => {
        expect(opt1.value).toEqual('hejpådej hur mår du?');
        expect(opt1.name).toEqual('test');
        called = true;
        return Promise.resolve();
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '-t="hejpådej hur mår du?"'
      ]);
    });

  });
});
