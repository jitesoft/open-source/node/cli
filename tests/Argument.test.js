import Manager from '../src/Manager';
import Argument from '../src/Argument';
import Command from '../src/Command';

describe('Arguments tests.', () => {
  describe('Test Setters/Getters', () => {
    const arg = new Argument('TestArg', 'Test description!', false, String, [
      'abc',
      '123',
      'HEJ!'
    ]);

    test('Name', () => {
      expect(arg.name).toEqual('TestArg');
    });

    test('Description', () => {
      expect(arg.description).toEqual('Test description!');
    });

    test('Expected type', () => {
      expect(arg.expectedType).toEqual(String);
    });

    test('Required', () => {
      expect(arg.required).toBe(false);
      const arg2 = new Argument('TestArg', 'Test description!', true);
      expect(arg2.required).toBe(true);
      const arg3 = new Argument('TestArg', '');
      expect(arg3.required).toBe(true);
    });

    test('Value', () => {
      expect(arg.value).toBeNull();
      arg.value = 'abc123';
      expect(arg.value).toEqual('abc123');
    });
  });

  describe('Test handle command with arguments.', () => {
    let _manager;
    let called = false;

    beforeEach(() => {
      _manager = new Manager(
        'TestManager', 'A manager used for testing.'
      );
      called = false;
    });

    afterEach(() => {
      expect(called).toEqual(true);
    });

    test('Argument without " should succeed.', async () => {
      const arg = new Argument('Test', 'adsc');
      const cmd = new Command('TestCommand', 'Test command.', { args: [arg] }, async (c, i, a, o) => {
        expect(a).toHaveLength(1);
        expect(a[0].name).toEqual('Test');
        expect(a[0].value).toEqual('abc');
        called = true;
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        'abc'
      ]);
    });

    test('Multi-word argument with "" should succeed.', async () => {
      const arg = new Argument('Test', 'adsc');
      const cmd = new Command('TestCommand', 'Test command.', { args: [arg] }, async (c, i, a, o) => {
        expect(a).toHaveLength(1);
        expect(a[0].name).toEqual('Test');
        expect(a[0].value).toEqual('abc efg hij');
        called = true;
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        '"abc efg hij"'
      ]);
    });

    test('Call without a optional argument should succeed.', async () => {
      const arg = new Argument('Test', 'adsc', false);
      const cmd = new Command('TestCommand', 'Test command.', { args: [arg] }, async (c, i, a, o) => {
        expect(a).toHaveLength(0);
        called = true;
      });

      _manager.register(cmd);
      await _manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand'
      ]);
    });

    test('Multi-word argument without " should fail.', async () => {
      const arg = new Argument('Test', 'adsc');
      const cmd = new Command('TestCommand', 'Test command.', { args: [arg] }, async (c, i, a, o) => {});
      called = true; // To avoid failure on called.
      _manager.register(cmd);
      await expect(_manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand',
        'test', 'abc'
      ])).rejects.toThrow('Unknown argument `abc`.');
    });

    test('Call without a required argument should fail.', async () => {
      const arg = new Argument('Test', 'adsc');
      const cmd = new Command('TestCommand', 'Test command.', { args: [arg] }, async (c, i, a, o) => {});
      called = true; // To avoid failure on called.
      _manager.register(cmd);
      await expect(_manager.invoke([
        'node.exe',
        '@jitesoft/cli',
        'TestCommand'
      ])).rejects.toThrow('Missing required argument `Test`.');
    });
  });
});
